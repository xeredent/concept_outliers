# Please read here

This repository still exists as part of an experiment published in [3].
All the algorithms tested here are implemented in a cleaner and more 
easily accessible library, 
[libmlconcepts](https://github.com/xeredent/libmlconcepts).
This library can be found both in
[github](https://github.com/xeredent/libmlconcepts) (the main repository) and
in its mirror in 
[gitlab](https://gitlab.com/xeredent/libmlconcepts).


# Concept Outliers

*Concept outliers* is the implementation of an outlier detection algorithm that
stems from ideas in [[1]](https://arxiv.org/abs/2210.17330) and 
[[2]](https://arxiv.org/abs/2301.01837), and that was benchmarked in [[3]](). An
 online (unfortunately slow) version of `concept_outliers` is available 
[here](https://colab.research.google.com/drive/1kl9l8-2zlkjqMeNpSrMBG9Z3YUe9x4C0?usp=sharing).


## Installation

*Concept outliers* requires `cmake` (version >= 3.1), and `eigen` 
(version >= 3.4).  To compile the program clone the repository, run `cmake`, and
build the resulting makefile.  Any c++ compiler supporting `c++20` works 
(see [here](#compile-with-older-c++-versions) for information on how to compile with older versions).
In short:

```sh
git clone https://gitlab.com/xeredent/concept_outliers.git
mkdir concept_outliers/build
cd concept_outliers/build
cmake ..
make
```

If your `eigen` installation is not standard, you can specify an arbitrary 
folder in the `cmake` command as follows:

```sh
cmake .. -DEigen3_DIR=$HOME/mypackagesfolder/eigen3/cmake
``` 

As a result of the build, a file `concept_outliers` will be generated in the
build folder. Put the built file and the file 
`concept_outliers/scripts/coutliers.py` in the same folder to run tests in bulk
as in [[3]](). 

If you want to compile a more memory-efficient (yet slower) version of the
 program, define the variable `PARTIAL_CONTEXT_INCIDENCE_ENTRY_USE_UNIQUE_PTR`.
See the [section on memory consumption](#memory-consumption).

## Development notes
The code has gone through a lot of changes in its lifetime, and no major 
refactoring has ever been done. I know I should undertake this small endeavor 
(very small, given the size of the project). A few notes on some possible 
optimizations follow.

### Memory consumption

*Concept outliers* consumes A LOT of memory. Formal contexts are essentially
implemented as pairs of matrices, one the transpose of the other, which are
*vectors* of sequences of bits. These sequences of bits are implemented by the 
class `PartialContextIncidenceEntry<T>`, where `T` defaults to `uint64_t`, which
represent arrays of some integer type which allow to perform operations such as
taking intersections efficiently. 

Since *concept outliers* generates `nbins` features for each attribute in the 
dataset, the occupied memory by each context is *at least* (in bytes)

```
nattrs * nbins * ceil(nobjs / 64) * 8 +
nobjs * ceil(nattrs * nbins / 64) * 8
```

where `nobjs` is the number of objects (entries) in the dataset, and `nattrs` is
the number of attributes (columns). So, for instance, for a dataset with `300000`
elements and `30` features, if `nbins=12`, *at least* `28MB` are required *for 
each* dataset. If the *doubletons* contexts method is used (the most memory heavy,
but slightly better performing), the program needs to compute

```
nattrs * (nattrs + 1) / 2
```

formal contexts at least, i.e., `465` contexts, for whopping total of *at least* `13GB`.

The *singleton* contexts mode helps in fighting this high memory requirements in exchange
of some performance, requiring to compute only `30` contexts, one for each feature
(totalling *at least* `840MB`) of memory. This is paid with an average of 4-5% of
the AUC score in the examined test cases.

There are at least two possible optimizations memory-wise:

- considering that the bit vectors in `PartialContextIncidenceEntry<T>` are 
implemented via `std::vector<T>`, it is possible to use different alternatives
whose `capacity` matches the `size` (as unfortunately `shrink_to_fit()` is 
platform-dependent and does not always make the two values the same). There are 
several solutions:
	- using `std::unique_ptr<T[]>`. This can be enabled by compiling with 
`PARTIAL_CONTEXT_INCIDENCE_ENTRY_USE_UNIQUE_PTR`. It does indeed save memory,
although, for some mysterious reason, the performance is worse than the one of
`std::vector`. I will profile the code in the future to find out what is 
going on.
	- write a custom allocator for `std::vector` that uses the least amount of
memory. We do not need the amortized complexity guarantees of (most of?) the default 
implementations.
	- hope that at some point people will actually implement `std::dynarray`. 
I mean, I see why people might not want it in the standard, but it would have
been a good use case here.
- Stop storing the incidence matrix and its transpose, but store just one of the
two. More specifically, store only a vector of objects for each feature. This 
would allow to compute the intersection of the extents of different features in 
a fast way, but I think that performance could by degraded due
 to cache locality being lost.
My hunch is that as our formal contexts are *very sparse* (for each attribute in 
the dataset, we have `nbins` features in the context, and each object is in relation with
just one of them), probably it is just better to avoid loading different register-sized
integers for each one of these contiguous attributes. Only benchmarks will tell if
I am correct.

### Compile with older c++ versions
The kind of hard requirement of `c++20` stems from the usage of the beautiful
and platform independent `std::popcount` to compute the size of a closure after 
computing the intersection of the extensions of several attributes. This can be
easily fixed by writing a small `asm` snippet doing popcount in its stead. For the
rest, `c++14` should be enough. 

# References
Not in any specific reference format. But I do provide the links.

[[1] Flexible categorization for auditing using formal concept analysis and 
Dempster-Shafer theory](https://arxiv.org/abs/2210.17330)

[[2] A Meta-Learning Algorithm for Interrogative Agendas](https://arxiv.org/abs/2301.01837)

[[3] Outlier detection using flexible categorisation and interrogative agendas]()
