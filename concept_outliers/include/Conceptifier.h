#ifndef CONCEPTIFIER_H
#define CONCEPTIFIER_H
#include <unordered_map>
#include <vector>
#include "FeatureAssigner.h"
#include "rapidcsv.h"
#include "PartialContext.h"

class Conceptifier
{
    std::unordered_map<std::string, FeatureAssigner*> assigners;
    std::vector<std::string> attrNames;
    std::vector<std::vector<bool>> contextAllowedFeatures;
    int nattrs; 
    int logLevel;
    
public:
    Conceptifier(int logLevel = 1);
    ~Conceptifier();
    
    void addAssigner(std::string s, FeatureAssigner* f);
    inline std::vector<std::string>& getAttrNames() { return attrNames; }
    void computeContext(rapidcsv::Document& data, std::vector<std::string>& features, std::vector<PartialContext>& contexts);
    void computeSingletonContexts(rapidcsv::Document& data, std::vector<std::string>& features, 
                                  std::vector<PartialContext>& contexts, bool skipFullLattice = false);
    void computeContexts(rapidcsv::Document& data, std::vector<std::string>& features, std::vector<PartialContext>& contexts,
                         bool skipFullLattice = false);
    
    void computeOutlierDegrees(rapidcsv::Document& data, std::vector<std::string>& features, std::vector<PartialContext>& contexts, 
                               PartialContextIncidenceEntry<uint64_t>& inliers, bool removeKnownOutliers, std::vector<double>& gammas,
                               std::vector<std::vector<double>>& outlierdegs, std::vector<std::vector<double>>& closures);
    void computeOutlierDegreesTest(rapidcsv::Document& data, std::vector<std::string>& features, std::vector<PartialContext>& contexts, 
                                   PartialContextIncidenceEntry<uint64_t>& inliers, bool removeKnownOutliers, std::vector<double>& gammas,
                                   std::vector<std::vector<double>>& outlierdegs, std::vector<std::vector<double>>& closures);
                     
    void computeClosures(rapidcsv::Document& data, std::vector<std::string>& features, std::vector<PartialContext>& contexts, 
                               PartialContextIncidenceEntry<uint64_t>& inliers, bool removeKnownOutliers,
                               std::vector<std::vector<double>>& closures);
    void computeClosuresTest(rapidcsv::Document& data, std::vector<std::string>& features, std::vector<PartialContext>& contexts, 
                                   PartialContextIncidenceEntry<uint64_t>& inliers, bool removeKnownOutliers,
                                   std::vector<std::vector<double>>& closures);
                                   
    
    void computeClosuresUnsupervised(rapidcsv::Document& data, std::vector<std::string>& features, std::vector<PartialContext>& contexts, 
                               std::vector<std::vector<double>>& closures);
    void computeClosuresTestUnsupervised(rapidcsv::Document& data, std::vector<std::string>& features, std::vector<PartialContext>& contexts, 
                                   std::vector<std::vector<double>>& closures);
    
    inline int getFeatureCount() { return nattrs; }
};

#endif // CONCEPTIFIER_H
