#ifndef FEATUREASSIGNER_H
#define FEATUREASSIGNER_H
#include "rapidcsv.h"
#include <algorithm>
#include <cmath>
#include <unordered_map>

enum class FeatureType {
    String,
    Number,
};

class FeatureAssigner
{
protected:
    std::string feature;
    int offset;
    
public:
    FeatureAssigner(std::string feature, int offset = 0);
    ~FeatureAssigner();
    
    inline void setOffset(int v) { offset = v; }
    virtual int assign(std::string& v) = 0;
    virtual int assign(double v) = 0;
    virtual int nfeatures() = 0;
    virtual FeatureType getType() = 0;
    inline int safeAssign(double v) {
        int b = assign(v);
        if (b < offset) return offset;
        if (b > offset + nfeatures() - 1) return offset + nfeatures() - 1;
        return b;
    }
    inline int safeAssign(std::string& v) {
        return assign(v);
    }
    inline std::string getFeatureName() { return feature; }
};

class UniformAssigner : public FeatureAssigner {
    int nbins;
    double min;
    double max;
public:
    UniformAssigner(rapidcsv::Document& data, std::string feature, int nbins) : FeatureAssigner(feature) {
        this->nbins = nbins;
        auto col = data.GetColumn<double>(feature);
        min = *std::min_element(col.begin(), col.end());
        max = *std::max_element(col.begin(), col.end());
    }
    
    int assign(std::string& v) override;
    int assign(double v) override;
    inline int nfeatures() override {
        return nbins;
    }
    inline FeatureType getType() override {
        return FeatureType::Number;
    }
};

class QuantileAssigner : public FeatureAssigner {
    
    std::vector<double> fquants;
    
    int get_quantile_id(double x);
    
public:
    QuantileAssigner(rapidcsv::Document& data, std::string feature, std::vector<double> quantiles) : FeatureAssigner(feature) {
        auto col = data.GetColumn<double>(feature);
        computeQuantiles(col, quantiles);
    }
    
    void computeQuantiles(std::vector<double>& col, std::vector<double> quantiles);
    
    int assign(std::string& v) override;
    int assign(double v) override;
    
    inline int nfeatures() override {
        return fquants.size();
    }
    inline FeatureType getType() override {
        return FeatureType::Number;
    }
};

class GeometricQuantileAssigner : public QuantileAssigner {

public:
    GeometricQuantileAssigner(rapidcsv::Document& data, std::string feature, int nsubdivisions, double invratio) : QuantileAssigner(data, feature, {})
    {
        auto col = data.GetColumn<double>(feature);
        std::vector<double> quantiles = { 0 };
        double denominator = std::pow(invratio, nsubdivisions);
        double accum = 0;
        for (int i = 0; i < nsubdivisions; ++i) {
            accum = accum + 1 / denominator;
            quantiles.push_back(accum);
            denominator /= invratio;
        }
        accum = accum + 1;
        quantiles.push_back(accum);
        denominator = invratio;
        for (int i = 0; i < nsubdivisions; ++i) {
            accum = accum + 1 / denominator;
            quantiles.push_back(accum);
            denominator *= invratio;
        }
        double norm = quantiles[quantiles.size() - 1];
        for (size_t i = 0; i < quantiles.size(); ++i) {
            quantiles[i] /= norm;
        }
        computeQuantiles(col, quantiles);
    }
};

class ValueAssigner : public FeatureAssigner {
    std::unordered_map<std::string, int> map;
    int nvalues;
    
public:
    ValueAssigner(rapidcsv::Document& data, std::string feature) : FeatureAssigner(feature) {
        auto col = data.GetColumn<std::string>(feature);
        nvalues = 0;
        for (std::string& s : col) {
            if (map.count(s) == 0) {
                map[s] = nvalues;
                nvalues++;
            }
        }
    }
    
    int assign(std::string& v) override;
    int assign(double v) override;
    inline int nfeatures() override {
        return nvalues;
    }
    inline FeatureType getType() override {
        return FeatureType::String;
    }
};


#endif // FEATUREASSIGNER_H
