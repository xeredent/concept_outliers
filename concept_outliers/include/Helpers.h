#ifndef HELPERS_H
#define HELPERS_H
#include <stdio.h>
#include <iostream>
#include <chrono>
#include <string>
#include <vector>
#include <algorithm>
#include <cmath>

namespace _helpers {
    template<class T>
    void json_write(std::ostream& o, const T& value) {
        o << value;
    }
    
    template<>
    inline void json_write(std::ostream& o, const std::vector<double>& value) {
        o << "[";
        for (size_t i = 0; i < value.size()-1; ++i) {
            json_write<double>(o, value[i]);
            o << ",";
        }
        if (value.size() > 0) json_write<double>(o, value[value.size()-1]);
        o << "]";
    }
    
    template<>
    inline void json_write(std::ostream& o, const std::string& value) {
        o << "\"" << value << "\"";
    }
    
    template<class T>
    inline void json_write(std::ostream& o, const std::vector<T>& value) {
        o << "[";
        for (int i = 0; i < (int)value.size()-1; ++i) {
            json_write<T>(o, value[i]);
            o << ",";
        }
        if (value.size() > 0) json_write<T>(o, value[value.size()-1]);
        o << "]";
    }
}

class Helpers
{
public:
    Helpers();
    ~Helpers();

    inline static void WriteTime(std::string s, std::chrono::duration<double> t, FILE* f = stdout) {
        fprintf(f, "%s ", s.c_str());
        double seconds = t.count();
        if (seconds > 120) { fprintf(f, "%dm %ds", (int)(seconds / 60.0), (int)std::round(seconds - (int)(seconds / 60.0))); }
        else if (seconds > 0.9) { fprintf(f, "%lgs", seconds); }
        else if (seconds > 0.0009) { fprintf(f, "%lgms", seconds*1000); }
        else if (seconds > 0.0000009) { fprintf(f, "%lgmicros", seconds*1000000); } 
        else { fprintf(f, "%lgns", seconds*1000000000); }
        fprintf(f, "\n");
    }
    
    static std::vector<std::string> split (const std::string &s, char delim);
    
    static inline void ltrim(std::string &s) {
    s.erase(s.begin(), std::find_if(s.begin(), s.end(), [](unsigned char ch) {
        return !std::isspace(ch);
    }));
    }

    // trim from end (in place)
    static inline void rtrim(std::string &s) {
        s.erase(std::find_if(s.rbegin(), s.rend(), [](unsigned char ch) {
            return !std::isspace(ch);
        }).base(), s.end());
    }

    // trim from both ends (in place)
    static inline void trim(std::string &s) {
        rtrim(s);
        ltrim(s);
    }
    
    template<class T>
    static inline void json_write(std::ostream& o, const T& value) { _helpers::json_write(o, value); }
    
    template<class T>
    static inline void json_write_pair(std::ostream& o, const std::string& key, const T& value) {
        json_write(o, key); o << ":"; json_write<T>(o, value);
    }
};

#endif // HELPERS_H
