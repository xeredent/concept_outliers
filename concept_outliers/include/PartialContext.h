#ifndef PARTIALCONTEXT_H
#define PARTIALCONTEXT_H
#include <vector>
#include <array>
#include <cstdint>
#include <string>
#include <numeric>
#include <memory>

//#define PARTIAL_CONTEXT_INCIDENCE_ENTRY_USE_UNIQUE_PTR

template <class T = uint64_t>
class PartialContextIncidenceEntry {
    const int bits = 8 * sizeof(T);
    const int mask = bits - 1;
    #ifdef PARTIAL_CONTEXT_INCIDENCE_ENTRY_USE_UNIQUE_PTR
    std::unique_ptr<T[]> data;
    size_t dataSize;
    #else
    std::vector<T> data;
    #endif

public:
    
    PartialContextIncidenceEntry(int n) {
        size_t size = n / bits + 1;
        #ifdef PARTIAL_CONTEXT_INCIDENCE_ENTRY_USE_UNIQUE_PTR
        data = std::make_unique<T[]>(size);
        dataSize = size;
        #else
        data.reserve(size);
        data.resize(size);
        data.shrink_to_fit();
        #endif
        for (size_t i = 0; i < size; ++i)
            data[i] = 0;
    }
    
    inline size_t size() const { 
        #ifdef PARTIAL_CONTEXT_INCIDENCE_ENTRY_USE_UNIQUE_PTR
        return dataSize;
        #else
        return data.size(); 
        #endif
    }
    inline int get_index(int i) const { return (i / bits); }
    inline int get_bit(int i) const { return i & mask; }
    inline void set(int i) { data[get_index(i)] |= ((T)1 << get_bit(i));  }
    inline bool get(int i) const { return data[get_index(i)] & ((T)1 << get_bit(i)); }
    inline T getWord(int i) const { return data[i]; }
    
    inline bool subsetOf(PartialContextIncidenceEntry& b) const {
        for (size_t i = 0; i < size(); ++i) {
            if (data[i] != (data[i] & b.data[i]))
                return false;
        }
        return true;
    }
    
    inline void meet(T* p) {
        for (size_t i = 0; i < size(); ++i) {
            p[i] &= data[i];
        }
    } 
    
    inline size_t getCount() const {
        #ifdef PARTIAL_CONTEXT_INCIDENCE_ENTRY_USE_UNIQUE_PTR
        size_t v = 0;
        for (size_t i = 0; i < size(); ++i) {
            v += std::popcount(data[i]);
        }
        return v;
        #else
        return std::accumulate(data.begin(), data.end(), 0, [](T a, T b){ return a + std::popcount(b); }); 
        #endif
    }
    
    inline void print(FILE* f = stdout) const { for (size_t i = 0; i < size(); ++i) fprintf(f, "%016lX", data[i]); fprintf(f, "\n");  }

    inline size_t estimateSize() const {
        size_t sz = sizeof(*this);
        #ifdef PARTIAL_CONTEXT_INCIDENCE_ENTRY_USE_UNIQUE_PTR
        sz += dataSize * sizeof(T);
        #else
        sz += data.capacity() * sizeof(T);
        #endif
        return sz;
    }
};



class PartialContext
{
    int nobjs;
    int nattrs;
    
    std::vector<std::string> attrNames;
    std::vector<std::string> objNames;
    std::vector<PartialContextIncidenceEntry<uint64_t>> incidence;
    std::vector<PartialContextIncidenceEntry<uint64_t>> incidenceConv; 
    
    uint64_t* closureUtility;
    size_t closureUtilitySize;
    bool ownClosurePtr;
    
public:
    PartialContext(int nobjs, int nattrs);
    PartialContext(PartialContext&& ctx);
    ~PartialContext();
    
    inline std::string getObjName(int obj) const { 
        if ((size_t)obj < objNames.size()) return objNames[obj]; else return ""; 
    }
    inline std::string getAttrName(int attr) const { 
        if ((size_t)attr < attrNames.size()) return attrNames[attr]; else return ""; 
    }
    
    inline void setObjName(int obj, std::string s) { 
        if (objNames.size() <= (size_t)obj) objNames.resize(obj + 1, "");
        objNames[obj] = s; 
    }
    inline void setAttrName(int attr, std::string s) { 
        if (attrNames.size() <= (size_t)attr) attrNames.resize(attr + 1, "");
        attrNames[attr] = s; }
    inline void setAttrsNames(std::vector<std::string>& v) { 
        if (attrNames.size() <= v.size()) attrNames.resize(v.size(), "");
        for (int i = 0; i < std::max(nattrs,(int)v.size()); ++i) attrNames[i] = v[i]; 
    }
    
    inline size_t estimateSize() const {
        size_t sz = sizeof(PartialContext);
        for (auto& e : incidence) sz += e.estimateSize();
        for (auto& e : incidenceConv) sz += e.estimateSize();
        for (auto& s : objNames) sz += s.capacity() * sizeof(std::string::value_type);
        for (auto& s : attrNames) sz += s.capacity() * sizeof(std::string::value_type);
        return sz;
    }
    
    size_t sizeOfClosure1(PartialContextIncidenceEntry<uint64_t>& attrs);
    inline size_t sizeOfClosure1(int objID) { return sizeOfClosure1(incidence[objID]); }
    size_t sizeOfClosure2(PartialContextIncidenceEntry<uint64_t>& attrs);
    inline size_t sizeOfClosure2(int objID) { return sizeOfClosure2(incidence[objID]); }
    
    inline size_t sizeOfClosure(PartialContextIncidenceEntry<uint64_t>& attrs) { return sizeOfClosure2(attrs); }
    inline size_t sizeOfClosure(int objID) { return sizeOfClosure2(objID); }
    
    size_t sizeOfClosureInliers1(PartialContextIncidenceEntry<uint64_t>& attrs, PartialContextIncidenceEntry<uint64_t>& inliers);
    inline size_t sizeOfClosureInliers1(int objID, PartialContextIncidenceEntry<uint64_t>& inliers) { return sizeOfClosureInliers1(incidence[objID], inliers); }
    size_t sizeOfClosureInliers2(PartialContextIncidenceEntry<uint64_t>& attrs, PartialContextIncidenceEntry<uint64_t>& inliers);
    inline size_t sizeOfClosureInliers2(int objID, PartialContextIncidenceEntry<uint64_t>& inliers) { return sizeOfClosureInliers2(incidence[objID], inliers); }
    
    inline size_t sizeOfClosureInliers(PartialContextIncidenceEntry<uint64_t>& attrs, PartialContextIncidenceEntry<uint64_t>& inliers) { return sizeOfClosureInliers2(attrs, inliers); }
    inline size_t sizeOfClosureInliers(int objID, PartialContextIncidenceEntry<uint64_t>& inliers) { return sizeOfClosureInliers2(objID, inliers); }
    
    inline void setIncidence(int obj, int attr) {  incidence[obj].set(attr); incidenceConv[attr].set(obj); }
    inline PartialContextIncidenceEntry<uint64_t>& getIncidence(int obj) { return incidence[obj]; }
    inline std::vector<std::string> getIncidenceNames(int obj) const { 
        std::vector<std::string> names;
        for (int i = 0; i < nattrs; ++i) if (incidence[obj].get(i)) names.push_back(getAttrName(i));
        return names;
    }
};

#endif // PARTIALCONTEXT_H
