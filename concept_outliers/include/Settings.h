#ifndef SETTINGS_H
#define SETTINGS_H
#include <string>
#include <unordered_map>
#include <sstream>
#include <fstream>
#include "Helpers.h"

namespace _settings_helpers {
    template<class T> T get(const std::string& v) { 
        std::istringstream ss(v);
        T t;
        ss >> t;
        return t; 
    }
    
    template<> inline std::vector<std::string> get(const std::string& v) {
        auto vec = Helpers::split(v, ',');
        for (auto& e : vec) Helpers::trim(e);
        return vec;
    }
}

class Settings
{
    std::unordered_map<std::string,std::string> map;
    
public:
    Settings();
    ~Settings();
    
    inline void initialize() {
        set("Supervised", "true");
        set("TrainSet", "train.csv");
        set("TestSet", "test.csv");
        set("OutputFile", "closures.bin");
        set("UniformAssignerBins", "32");
        set("Features", "");
        set("ValueAssigners", "");
        set("OutlierColumn", "y");
        set("MaxTrainEpochs", "20000"); 
        set("LearningRate", "0.01"); 
        set("Momentum", "0.01"); 
        set("ConceptifierLogLevel", "2");
        set("TrainStopThreshold", "0.05");
        set("MaxGammaTrainEpochs", "2000"); 
        set("GammaLearningRate", "0.01"); 
        set("GammaMomentum", "0.01"); 
        set("GammaTrainStopThreshold", "0.05");
        set("OnlySingletonContexts", "false");
        set("SkipFullLattice", "false");
        set("GammaValues", "random"); //random, train, or value
    }
    
    inline bool contains(const std::string& s) const { return map.count(s) != 0; }
    
    inline void set(const std::string& key, const std::string& value) {
        map[key] = value;  
    }
    
    template <class T> T get(const std::string& s) {
        if (map.count(s) == 0) throw std::invalid_argument("Setting `" + s + "' does not exist.");
        return _settings_helpers::get<T>(map[s]);
    }
    
    
    inline bool getBool(const std::string& s) {
        auto v = get<std::string>(s);
        return v == "true" || v == "True" || v == "TRUE" || v == "1";
    }
    
    void ReadFile(std::ifstream& fstream);
    void ReadFile(const std::string& filename);
};


#endif // SETTINGS_H
