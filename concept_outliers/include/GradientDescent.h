#ifndef GRADIENTDESCENT_H
#define GRADIENTDESCENT_H
#include <functional>
#include <vector>
#include <Eigen/Core>

class GradientDescent
{
    typedef Eigen::VectorXd vec;
    typedef std::function<double(vec&)> wfun;
    typedef std::function<vec(vec&)> gfun;
    wfun loss;
    gfun gradient;
    vec weights;
    vec prevStep;
    double momentum;
    double learningRate;
    double improvementStopThreshold;
    
public:
    GradientDescent(int weightCount, wfun loss, gfun gradient, double learningRate = 0.01,
                    double momentum = 0.01, double improvementStopThreshold = 0.0);
    ~GradientDescent();
    
    void computeIteration();

    inline void setWeights(double v) { for (auto& w : weights) w = v; }
    inline double getLoss() { return loss(weights); } 
    inline vec& getWeights() { return weights; }
    
    inline size_t train(size_t maxEpochs, bool writeLog = false) {
        double prevLoss = getLoss();
        if (writeLog) printf("Starting training (loss %lg)...", prevLoss);
        for (size_t epoch = 0; epoch < maxEpochs; ++epoch) { 
            computeIteration();
            double newLoss = getLoss();
            if (writeLog) { printf("Train epoch %lu, loss %lg                        \r", epoch + 1, newLoss); fflush(stdout); }
            if (prevLoss - newLoss < improvementStopThreshold && prevLoss - newLoss >= 0.0) return epoch + 1;
            prevLoss = newLoss;
        }
        if (writeLog) printf("\n");
        return maxEpochs;
    }
};

#endif // GRADIENTDESCENT_H
