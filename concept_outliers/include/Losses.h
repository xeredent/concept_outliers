#include "PartialContext.h"
#include "GradientDescent.h"
#include <numeric>


namespace losses {
    template <class Function>
    void getOutdegsArray(const std::vector<std::vector<double>>& closures, std::vector<Eigen::VectorXd>& outdegs, 
                         const std::vector<double>& gammas, Function outdegfunction) {
        if (closures.size() == 0) return;
        size_t nagendas = closures.size();
        for (size_t i = 0; i < closures[0].size(); ++i) {
            outdegs.push_back(Eigen::ArrayXd::Zero(nagendas));
            Eigen::VectorXd& v = outdegs[outdegs.size() - 1];
            for (size_t j = 0; j < nagendas; ++j) {
                v(j) = outdegfunction(closures[j][i], gammas[j]);
            }
        }
    }
    
    inline double getBalance(int nobjs, const PartialContextIncidenceEntry<uint64_t>& inliers) {
        return (double)nobjs / (double)(nobjs - inliers.getCount());
    }
    
    inline std::function<double(Eigen::VectorXd&)> getTrainLoss(const std::vector<Eigen::VectorXd>& outdegs,
                                                               const PartialContextIncidenceEntry<uint64_t>& inliers) {
        int nobjs = outdegs.size(); int nagendas = nobjs == 0 ? 0 : outdegs[0].size();
        double balance = getBalance(nobjs, inliers);
        return [nobjs, nagendas, balance, outdegs, & inliers](Eigen::VectorXd& weights){
            double Wp = std::accumulate(weights.begin(), weights.end(), 0.0, 
                                        [](double a, double b){ return b > 0 ? a + b : a; } ) ;
            double Wm = std::accumulate(weights.begin(), weights.end(), 0.0, 
                                        [](double a, double b){ return b < 0 ? a + b : a; } ) ;
            double Wpm = Wp + Wm;
            double sum = 0;
            for (int i = 0; i < nobjs; ++i) {
                double ycap = (weights.dot(outdegs[i]) - Wm) / Wpm;
                sum += inliers.get(i) ? std::pow(ycap,2)/balance : std::pow((1 - ycap), 2); 
            }
            return sum;
        };
    }
    
    inline std::function<Eigen::VectorXd(Eigen::VectorXd&)> getTrainGradient(const std::vector<Eigen::VectorXd>& outdegs,
                                                               const PartialContextIncidenceEntry<uint64_t>& inliers) {
        int nobjs = outdegs.size(); int nagendas = nobjs == 0 ? 0 : outdegs[0].size();
        double balance = getBalance(nobjs, inliers);
        return [nobjs, nagendas, balance, outdegs, & inliers](Eigen::VectorXd& weights) {
            double Wp = std::accumulate(weights.begin(), weights.end(), 0.0, 
                                        [](double a, double b){ return b > 0 ? a + b : a; } ) ;
            double Wm = std::accumulate(weights.begin(), weights.end(), 0.0, 
                                        [](double a, double b){ return b < 0 ? a + b : a; } ) ;
            double Wpm = Wp + Wm;
            Eigen::VectorXd output = Eigen::VectorXd::Zero(nagendas);
            for (int k = 0; k < nagendas; ++k) {
                double sum = 0.0;
                for (int i = 0; i < nobjs; ++i) {
                    double dot = weights.dot(outdegs[i]);
                    double ycap = (dot - Wm) / Wpm;
                    double dycap = weights(k) < 0 ? ((outdegs[i](k) - 1) * Wpm - dot) / std::pow(Wpm, 2.0) :
                                                    ((outdegs[i](k) - 1) * Wpm - dot) / std::pow(Wpm, 2.0);
                    sum += inliers.get(i) ?  2*(ycap)*1/balance*dycap : (2*(1 - ycap))*(-dycap);
                }
                output(k) = sum;
            }
            return output;
        };
    }
    
    template<class Function>
    inline std::function<double(Eigen::VectorXd&)> getGammaTrainLoss(const std::vector<double>& closures,
                                                    const PartialContextIncidenceEntry<uint64_t>& inliers,
                                                    Function outdegfun) {
        int nobjs = closures.size();
        double balance = getBalance(nobjs, inliers);
        return [nobjs, closures, & inliers, balance, outdegfun](Eigen::VectorXd& weights){
            double sum = 0.0;
            double gamma = weights(0);
            for (size_t i = 0; i < closures.size(); ++i) {
                sum += inliers.get(i) ? std::pow(outdegfun(closures[i], gamma), 2.0)/balance : 
                                        std::pow(1 - outdegfun(closures[i], gamma) , 2.0);
            }
            return sum;
        };
    }
    
    template<class Function>
    inline std::function<Eigen::VectorXd(Eigen::VectorXd&)> getGammaTrainGradient(const std::vector<double>& closures,
                                                    const PartialContextIncidenceEntry<uint64_t>& inliers,
                                                    Function outdegfun) {
        int nobjs = closures.size();
        double balance = getBalance(nobjs, inliers);
        return [nobjs, closures, & inliers, balance, outdegfun](Eigen::VectorXd& weights){
            double sum = 0.0;
            double gamma = weights(0);
            for (int i = 0; i < nobjs; ++i) {
                sum += inliers.get(i) ? (2.0 * outdegfun(closures[i], gamma)) * (-2.0 * gamma * std::pow(closures[i], 2.0)) / balance: 
                                        -4.0 * (1.0 - outdegfun(closures[i], gamma)) * gamma * std::pow(closures[i], 2.0);
                /*double var = inliers.get(i) ? (2.0 * outdegfun(closures[i], gamma)) * (-2.0 * gamma * std::pow(closures[i], 2.0)) / balance: 
                                        -4.0 * (1.0 - outdegfun(closures[i], gamma)) * gamma * std::pow(closures[i], 2.0);
                if (var != 0.0) {
                    printf("MEGA VARIATION\n");
                    printf("\tvariation %lg\n", var);
                    printf("\tinlier    %d\n", (int)inliers.get(i));
                    printf("\tgamma     %lg\n", gamma);
                    printf("\tclosure   %lg\n", closures[i]);
                    printf("\toutdegf   %lg\n", outdegfun(closures[i], weights(0)));
                    fflush(stdout);
                }*/
            }
            Eigen::VectorXd output = Eigen::VectorXd::Zero(1); output(0) = sum;
            return output;
        };
    }
}