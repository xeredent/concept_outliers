#include "Settings.h"
#include "Helpers.h"

Settings::Settings()
{
    this->initialize();
}

Settings::~Settings()
{
}



void Settings::ReadFile(std::ifstream & f) {
    std::string line;
    while (std::getline(f, line)) {
        std::vector<std::string> v = Helpers::split(line, '=');
        if (v.size() == 2) {
            Helpers::trim(v[0]); Helpers::trim(v[1]);
            this->set(v[0], v[1]);
        }
    }
}

void Settings::ReadFile(const std::string& filename) {
    std::ifstream fs(filename);
    ReadFile(fs);
}
