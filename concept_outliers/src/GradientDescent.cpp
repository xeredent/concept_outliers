#include "GradientDescent.h"
#include <random>
#include <chrono>
#include <iostream>

GradientDescent::GradientDescent(int weightCount, wfun loss, gfun gradient, double learningRate, double momentum, double improvementStopThreshold)
{
    weights = vec::Random(weightCount);
    for (auto& w : weights) std::abs(w);
    prevStep = vec::Zero(weightCount);
    this->loss = loss;
    this->gradient = gradient;
    this->learningRate = learningRate;
    this->momentum = momentum;
    this->improvementStopThreshold = improvementStopThreshold;
}

GradientDescent::~GradientDescent()
{
}

void GradientDescent::computeIteration()
{
    prevStep = momentum * prevStep - learningRate * gradient(weights);
    weights += prevStep;
}