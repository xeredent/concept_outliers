#include "Conceptifier.h"
#include <cmath>

Conceptifier::Conceptifier(int log)
{
    this->nattrs = 0;
    logLevel = log;
}

Conceptifier::~Conceptifier()
{
}



void Conceptifier::computeContext(rapidcsv::Document & data, std::vector<std::string>& features, std::vector<PartialContext>& contexts)
{
    contexts.push_back(PartialContext(data.GetRowCount(), nattrs));
    PartialContext& context = contexts[contexts.size() - 1];
    for (std::string& f : features) {
        FeatureAssigner* assigner = assigners[f];
        if (assigner->getType() == FeatureType::Number) {
            auto column = data.GetColumn<double>(f);
            for (size_t i = 0; i < column.size(); ++i) 
                context.setIncidence(i, assigner->assign(column[i]));
        }
        else if (assigner->getType() == FeatureType::String) {
            auto column = data.GetColumn<std::string>(f);
            for (size_t i = 0; i < column.size(); ++i) {
                context.setIncidence(i, assigner->assign(column[i]));
            }
        }
    }
}

void Conceptifier::computeSingletonContexts(rapidcsv::Document & data, std::vector<std::string>& features, 
                                            std::vector<PartialContext>& contexts, bool skipFullLattice)
{
    std::vector<std::string> attrs = {""};
    size_t nagendas = features.size();
    size_t totalSize = 0;
    contextAllowedFeatures.clear();
    for (size_t i = 0; i < nagendas; ++i) {
        contextAllowedFeatures.push_back(std::vector<bool>()); 
        std::vector<bool>& allowed = contextAllowedFeatures[contextAllowedFeatures.size() - 1];
        for (size_t j = 0; j < features.size(); ++j) allowed.push_back(i == j);
        attrs[0] = features[i];
        computeContext(data, attrs, contexts);
        if (logLevel >= 1) {
            if (logLevel >= 3) {
                totalSize += contexts[contexts.size() - 1].estimateSize();
                printf("Computed context %3lu/%3lu    %lgMB     %s", i + 1, 
                        nagendas, totalSize/1000000.0, (i == nagendas - 1) ? "\n" : "\r");
            } else {
                printf("Computed context %3lu/%3lu%s", i + 1, 
                        nagendas, (i == nagendas - 1) ? "\n" : "\r");
            }
            if (logLevel >= 2) fflush(stdout);
        }
    }
    if (!skipFullLattice) {
        contextAllowedFeatures.push_back(std::vector<bool>()); std::vector<bool>& allowed = contextAllowedFeatures[contextAllowedFeatures.size() - 1];
        for (size_t i = 0; i < features.size(); ++i) allowed.push_back(true);
        computeContext(data, features, contexts);
    }
}

void Conceptifier::computeContexts(rapidcsv::Document & data, std::vector<std::string>& features, 
                                    std::vector<PartialContext>& contexts, bool skipFullLattice)
{
    size_t f1 = 0;
    size_t f2 = 0;
    std::vector<std::string> attrs1 = {""};
    std::vector<std::string> attrs2 = {"", ""};
    size_t nagendas = features.size() * (features.size()  + 1) / 2;
    size_t totalSize = 0;
    contextAllowedFeatures.clear();
    for (size_t i = 0; i < nagendas; ++i) {
        contextAllowedFeatures.push_back(std::vector<bool>()); std::vector<bool>& allowed = contextAllowedFeatures[contextAllowedFeatures.size() - 1];
        for (size_t j = 0; j < features.size(); ++j) allowed.push_back(f1 == j || f2 == j);
        if (f2 != f1) {
            attrs2[0] = features[f1];
            attrs2[1] = features[f2];
            computeContext(data, attrs2, contexts);
        } else {
            attrs1[0] = features[f1];
            computeContext(data, attrs1, contexts);
        }
        f1++;
        if (f1 >= features.size() ) {
            f2++;
            f1 = f2; 
        }
        if (logLevel >= 1) {
            if (logLevel >= 3) {
                totalSize += contexts[contexts.size() - 1].estimateSize();
                printf("Computed context %3lu/%3lu    %lgMB     %s", i + 1, 
                        nagendas, totalSize/1000000.0, (i == nagendas - 1) ? "\n" : "\r");
            } else {
                printf("Computed context %3lu/%3lu%s", i + 1, 
                        nagendas, (i == nagendas - 1) ? "\n" : "\r");
            }
            if (logLevel >= 2) fflush(stdout);
        }
    }
    if (!skipFullLattice) {
        contextAllowedFeatures.push_back(std::vector<bool>()); std::vector<bool>& allowed = contextAllowedFeatures[contextAllowedFeatures.size() - 1];
        for (size_t i = 0; i < features.size(); ++i) allowed.push_back(true);
        computeContext(data, features, contexts);
    }
}

static inline double outdeg(double x, double gamma) {
    return std::exp(-std::pow(x * gamma, 2));
}

void Conceptifier::computeOutlierDegrees(rapidcsv::Document & data, std::vector<std::string>& features, std::vector<PartialContext>& contexts, 
                                         PartialContextIncidenceEntry<uint64_t>& inliers, bool removeKnownOutliers, std::vector<double>& gammas,
                                         std::vector<std::vector<double>>& outlierdegs, std::vector<std::vector<double>>& closures)
{
    for (size_t contextID = 0; contextID < contexts.size(); ++contextID) {
        double gamma = gammas[contextID];
        PartialContext& context = contexts[contextID];
        outlierdegs.push_back(std::vector<double>());
        closures.push_back(std::vector<double>());
        std::vector<double>& odegs = outlierdegs[outlierdegs.size() - 1];
        std::vector<double>& cls = closures[closures.size() - 1];
        for (size_t a = 0; a < data.GetRowCount(); ++a) {
            size_t clsize = context.sizeOfClosureInliers(a, inliers);
            odegs.push_back(outdeg(clsize, gamma));
            cls.push_back(clsize);
        }
        if (logLevel >= 1) {
            printf("Computed outdegs %3lu/%3lu\n", contextID + 1, contexts.size());
            if (logLevel >= 2) fflush(stdout);
        }
    }
}

void Conceptifier::computeOutlierDegreesTest(rapidcsv::Document & data, std::vector<std::string>& features, std::vector<PartialContext>& contexts, 
                                             PartialContextIncidenceEntry<uint64_t>& inliers, bool removeKnownOutliers, std::vector<double>& gammas,
                                             std::vector<std::vector<double>>& outlierdegs, std::vector<std::vector<double>>& closures)
{
    std::vector<int> featureIDs = std::vector<int>();
    std::vector<FeatureAssigner*> assigners = std::vector<FeatureAssigner*>();
    for (auto& f : features) {
        featureIDs.push_back(data.GetColumnIdx(f));
        assigners.push_back(this->assigners[f]);
    }
    for (size_t contextID = 0; contextID < contexts.size(); ++contextID) {
        double gamma = gammas[contextID];
        PartialContext& context = contexts[contextID];
        outlierdegs.push_back(std::vector<double>());
        closures.push_back(std::vector<double>());
        std::vector<double>& odegs = outlierdegs[outlierdegs.size() - 1];
        std::vector<double>& cls = closures[closures.size() - 1];
        for (size_t a = 0; a < data.GetRowCount(); ++a) {
            PartialContextIncidenceEntry<uint64_t> attrs(nattrs);
            bool empty = false;
            for (size_t f = 0; f < features.size(); ++f) {
                if (!contextAllowedFeatures[contextID][f]) continue;
                FeatureAssigner* assigner = assigners[f];
                int attrID = -1;
                if (assigner->getType() == FeatureType::Number) {
                    attrID = assigner->safeAssign(data.GetCell<double>(featureIDs[f], a));
                }
                else if (assigner->getType() == FeatureType::String) {
                    std::string s = data.GetCell<std::string>(featureIDs[f], a);
                    attrID = (assigner->assign(s));
                }
                if (attrID == -1) {
                    empty = true;
                    break;
                }
                attrs.set(attrID);
            }
            size_t clsize = 0;
            if (empty) {
                odegs.push_back(1.0);
            } else {
                clsize = context.sizeOfClosureInliers(attrs, inliers);
                odegs.push_back(outdeg(clsize, gamma));
            }
            cls.push_back(clsize);
        }
    }
}

void Conceptifier::addAssigner(std::string s, FeatureAssigner * f) {
    for (int i = 0; i < f->nfeatures(); ++i) {
        attrNames.push_back(s + " - " + std::to_string(i));
    }
    assigners[s] = f; 
    f->setOffset(nattrs); 
    nattrs += f->nfeatures();
}

void Conceptifier::computeClosures(rapidcsv::Document & data, std::vector<std::string>& features, std::vector<PartialContext>& contexts, 
                                         PartialContextIncidenceEntry<uint64_t>& inliers, bool removeKnownOutliers,
                                        std::vector<std::vector<double>>& closures)
{
    for (size_t contextID = 0; contextID < contexts.size(); ++contextID) {
        PartialContext& context = contexts[contextID];
        closures.push_back(std::vector<double>());
        std::vector<double>& cls = closures[closures.size() - 1];
        for (size_t a = 0; a < data.GetRowCount(); ++a) {
            size_t clsize = context.sizeOfClosureInliers(a, inliers);
            cls.push_back(clsize);
        }
        if (logLevel >= 1) {
            printf("Computed closures %3lu/%3lu%s", contextID + 1, contexts.size(), contextID == contexts.size() - 1 ? "\n" : "\r");
            if (logLevel >= 2) fflush(stdout);
        }
    }
}

void Conceptifier::computeClosuresTest(rapidcsv::Document & data, std::vector<std::string>& features, std::vector<PartialContext>& contexts, 
                                             PartialContextIncidenceEntry<uint64_t>& inliers, bool removeKnownOutliers,
                                            std::vector<std::vector<double>>& closures)
{
    std::vector<int> featureIDs = std::vector<int>();
    std::vector<FeatureAssigner*> assigners = std::vector<FeatureAssigner*>();
    for (auto& f : features) {
        featureIDs.push_back(data.GetColumnIdx(f));
        assigners.push_back(this->assigners[f]);
    }
    for (size_t contextID = 0; contextID < contexts.size(); ++contextID) {
        PartialContext& context = contexts[contextID];
        closures.push_back(std::vector<double>());
        std::vector<double>& cls = closures[closures.size() - 1];
        for (size_t a = 0; a < data.GetRowCount(); ++a) {
            PartialContextIncidenceEntry<uint64_t> attrs(nattrs);
            bool empty = false;
            for (size_t f = 0; f < features.size(); ++f) {
                if (!contextAllowedFeatures[contextID][f]) continue;
                FeatureAssigner* assigner = assigners[f];
                int attrID = -1;
                if (assigner->getType() == FeatureType::Number) {
                    attrID = assigner->safeAssign(data.GetCell<double>(featureIDs[f], a));
                }
                else if (assigner->getType() == FeatureType::String) {
                    std::string s = data.GetCell<std::string>(featureIDs[f], a);
                    attrID = (assigner->assign(s));
                }
                if (attrID == -1) {
                    empty = true;
                    break;
                }
                attrs.set(attrID);
            }
            size_t clsize = 0;
            if (!empty) {
                clsize = context.sizeOfClosureInliers(attrs, inliers);
            }
            cls.push_back(clsize);
        }
        if (logLevel >= 1) {
            printf("Computed test closures %3lu/%3lu%s", contextID + 1, contexts.size(), contextID == contexts.size() - 1 ? "\n" : "\r");
            if (logLevel >= 2) fflush(stdout);
        }
    }
}


void Conceptifier::computeClosuresUnsupervised(rapidcsv::Document & data, std::vector<std::string>& features, std::vector<PartialContext>& contexts, std::vector<std::vector<double>>& closures) {
    for (size_t contextID = 0; contextID < contexts.size(); ++contextID) {
        PartialContext& context = contexts[contextID];
        closures.push_back(std::vector<double>());
        std::vector<double>& cls = closures[closures.size() - 1];
        for (size_t a = 0; a < data.GetRowCount(); ++a) {
            size_t clsize = context.sizeOfClosure(a);
            cls.push_back(clsize);
        }
        if (logLevel >= 1) {
            printf("Computed closures %3lu/%3lu%s", contextID + 1, contexts.size(), contextID == contexts.size() - 1 ? "\n" : "\r");
            if (logLevel >= 2) fflush(stdout);
        }
    }
}

void Conceptifier::computeClosuresTestUnsupervised(rapidcsv::Document & data, std::vector<std::string>& features, std::vector<PartialContext>& contexts, std::vector<std::vector<double>>& closures)
{
    std::vector<int> featureIDs = std::vector<int>();
    std::vector<FeatureAssigner*> assigners = std::vector<FeatureAssigner*>();
    for (auto& f : features) {
        featureIDs.push_back(data.GetColumnIdx(f));
        assigners.push_back(this->assigners[f]);
    }
    for (size_t contextID = 0; contextID < contexts.size(); ++contextID) {
        PartialContext& context = contexts[contextID];
        closures.push_back(std::vector<double>());
        std::vector<double>& cls = closures[closures.size() - 1];
        for (size_t a = 0; a < data.GetRowCount(); ++a) {
            PartialContextIncidenceEntry<uint64_t> attrs(nattrs);
            bool empty = false;
            for (size_t f = 0; f < features.size(); ++f) {
                if (!contextAllowedFeatures[contextID][f]) continue;
                FeatureAssigner* assigner = assigners[f];
                int attrID = -1;
                if (assigner->getType() == FeatureType::Number) {
                    attrID = assigner->safeAssign(data.GetCell<double>(featureIDs[f], a));
                }
                else if (assigner->getType() == FeatureType::String) {
                    std::string s = data.GetCell<std::string>(featureIDs[f], a);
                    attrID = (assigner->assign(s));
                }
                if (attrID == -1) {
                    empty = true;
                    break;
                }
                attrs.set(attrID);
            }
            size_t clsize = 0;
            if (!empty) {
                clsize = context.sizeOfClosure(attrs);
            }
            cls.push_back(clsize);
        }
        if (logLevel >= 1) {
            printf("Computed test closures %3lu/%3lu%s", contextID + 1, contexts.size(), contextID == contexts.size() - 1 ? "\n" : "\r");
            if (logLevel >= 2) fflush(stdout);
        }
    }
}