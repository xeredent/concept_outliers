#include "PartialContext.h"
#include <bit>

PartialContext::PartialContext(int nobjs, int nattrs)
{
    this->nobjs = nobjs;
    this->nattrs = nattrs;
    for (int i = 0; i < nobjs; ++i) {
        incidence.push_back(PartialContextIncidenceEntry<uint64_t>(nattrs));
    }
    for (int i = 0; i < nattrs; ++i) {
        incidenceConv.push_back(PartialContextIncidenceEntry<uint64_t>(nobjs));
    }
    incidenceConv.shrink_to_fit();
    incidence.shrink_to_fit();
    closureUtilitySize = nobjs / (8 * sizeof(uint64_t)) + 1;
    closureUtility = new uint64_t[closureUtilitySize];
    ownClosurePtr = true;
}

 PartialContext::PartialContext(PartialContext && ctx) {
    nobjs = ctx.nobjs;
    nattrs = ctx.nattrs;
    ownClosurePtr = true;
    ctx.ownClosurePtr = false;
    incidence = std::move(ctx.incidence);
    incidenceConv = std::move(ctx.incidenceConv);
    attrNames = std::move(ctx.attrNames);
    closureUtilitySize = ctx.closureUtilitySize;
    closureUtility = ctx.closureUtility;
}

PartialContext::~PartialContext()
{
    if (ownClosurePtr) delete[] closureUtility;
}


size_t PartialContext::sizeOfClosure1(PartialContextIncidenceEntry<uint64_t>& attrs)
{
    size_t cnt = 0;
    for (int a = 0; a < nobjs; ++a) {
        auto& intension = incidence[a]; 
        if (attrs.subsetOf(intension))
            cnt++;
    }
    return cnt;
}

size_t PartialContext::sizeOfClosure2(PartialContextIncidenceEntry<uint64_t>& attrs)
{
    for (size_t i = 0; i < closureUtilitySize; ++i) closureUtility[i] = 0xFFFFFFFFFFFFFFFF;
    for (int x = 0; x < nattrs; ++x) {
        if (attrs.get(x)) {
            auto& extension = incidenceConv[x];
            extension.meet(closureUtility);
        }
    }
    size_t cnt = 0;
    for (size_t i = 0; i < closureUtilitySize; ++i) {
        cnt += std::popcount(closureUtility[i]);
    }
    return cnt;
}

size_t PartialContext::sizeOfClosureInliers1(PartialContextIncidenceEntry<uint64_t>& attrs, PartialContextIncidenceEntry<uint64_t>& inliers)
{
    size_t cnt = 0;
    for (int a = 0; a < nobjs; ++a) {
        auto& intension = incidence[a]; 
        if (inliers.get(a) && attrs.subsetOf(intension))
            cnt++;
    }
    return cnt;
}

size_t PartialContext::sizeOfClosureInliers2(PartialContextIncidenceEntry<uint64_t>& attrs, PartialContextIncidenceEntry<uint64_t>& inliers)
{
    for (size_t i = 0; i < closureUtilitySize; ++i) closureUtility[i] = 0xFFFFFFFFFFFFFFFF;
    for (int x = 0; x < nattrs; ++x) {
        if (attrs.get(x)) {
            auto& extension = incidenceConv[x];
            extension.meet(closureUtility);
        }
    }
    size_t cnt = 0;
    for (size_t i = 0; i < closureUtilitySize; ++i) {
        cnt += std::popcount(closureUtility[i] & inliers.getWord(i));
    }
    return cnt;
}

