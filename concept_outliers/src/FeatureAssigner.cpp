#include "FeatureAssigner.h"
#include <sstream>


FeatureAssigner::FeatureAssigner(std::string feature, int offset)
{
    this->feature = feature;
    this->offset = offset;
}
FeatureAssigner::~FeatureAssigner()
{
    
}

int UniformAssigner::assign(std::string & v)
{
    std::istringstream ss(v);
    double dv;
    ss >> dv;
    return assign(dv);
}

int UniformAssigner::assign(double v)
{
    int id = ((v - min) / (max - min)) * nbins;
    if (id >= nbins) id = nbins - 1;
    return offset + id;
}

static double getnth(std::vector<double>& col, int n) {
    auto nth = col.begin() + n;
    std::nth_element(col.begin(), nth, col.end());
    return *nth;
}

static double quantile(std::vector<double>& col, double q) {
    double dnth = q * (col.size()-1);
    int nth = (int)dnth;
    double a = getnth(col, nth);
    double b = getnth(col, nth+1);
    double f = dnth - nth;
    return a + (b - a) * f;
}

void QuantileAssigner::computeQuantiles(std::vector<double>& col, std::vector<double> quantiles)
{
    fquants.clear();
    for (auto q : quantiles) {
        fquants.push_back(quantile(col, q));
    }
}

int QuantileAssigner::assign(std::string & v)
{
    std::istringstream ss(v);
    double dv;
    ss >> dv;
    return assign(dv);
}

int QuantileAssigner::assign(double v)
{
    return offset + get_quantile_id(v);
}

int QuantileAssigner::get_quantile_id(double x)
{
    int start = 0; 
    int end = fquants.size();
    int i = end / 2;
    while (1) {
        if (x < fquants[i]) {
            end = i;
            i = (start + end) / 2;
        }
        else if (i == end - 1) return i;
        else if (x >= fquants[i + 1]) {
            start = i + 1;
            i = (start + end) / 2;
        }
        else return i;
    }
}

int ValueAssigner::assign(std::string & v)
{
    if (map.count(v) == 0) 
        return -1;
    return offset + map[v];
}

int ValueAssigner::assign(double v)
{
    return -1;
}