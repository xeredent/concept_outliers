#include <stdio.h>
#include <string.h>
#include <random>
#include "Settings.h"
#include "Conceptifier.h"
#include "Helpers.h"
#include "PartialContext.h"
#include "Losses.h"

void execute(Settings& settings) {
    int logLevel = settings.get<int>("ConceptifierLogLevel");
    std::chrono::duration<double> computeContextsTime(0.0);
    std::chrono::duration<double> computeTrainClosuresTime(0.0);
    std::chrono::duration<double> computeTestClosuresTime(0.0);
    std::chrono::duration<double> computeGammasTime(0.0);
    std::chrono::duration<double> computeTrainingTime(0.0);
    auto features = settings.get<std::vector<std::string>>("Features");
    auto valueAssignedFeatures = settings.get<std::vector<std::string>>("ValueAssigners");
    rapidcsv::Document dataset = rapidcsv::Document(settings.get<std::string>("TrainSet"));
    rapidcsv::Document testset = rapidcsv::Document(settings.get<std::string>("TestSet"));
    int nobjs = dataset.GetRowCount();
    
    bool supervised = settings.getBool("Supervised");
    PartialContextIncidenceEntry<uint64_t> inliers(dataset.GetRowCount());
    if (supervised) {
        auto outliers = dataset.GetColumn<int>(settings.get<std::string>("OutlierColumn"));
        for (size_t i = 0; i < outliers.size(); ++i) {
            if (outliers[i] == 0) 
                inliers.set(i);
        }
    }
    
    int uniformBins = settings.get<int>("UniformAssignerBins");
    Conceptifier conceptifier(logLevel);
    for (auto& f : features) {
        if (std::count(valueAssignedFeatures.begin(), valueAssignedFeatures.end(), f))
            conceptifier.addAssigner(f, new ValueAssigner(dataset, f));
        else conceptifier.addAssigner(f, new UniformAssigner(dataset, f, uniformBins));
    }
    
    bool onlySingletonContexts = settings.getBool("OnlySingletonContexts");
    bool skipFullLattice = settings.getBool("SkipFullLattice");
    std::vector<std::vector<double>> trainClosures;
    std::vector<std::vector<double>> testClosures;
    std::vector<PartialContext> contexts;
    int nagendas = (onlySingletonContexts ? features.size() : features.size() * (features.size() + 1) / 2 ) + 
                   (skipFullLattice ? 0 : 1);
    contexts.reserve(nagendas);
    
    // COMPUTE CLOSURES
    auto stTime = std::chrono::steady_clock::now();
    if (onlySingletonContexts)
        conceptifier.computeSingletonContexts(dataset, features, contexts, skipFullLattice);
    else conceptifier.computeContexts(dataset, features, contexts, skipFullLattice);
    auto enTime = std::chrono::steady_clock::now();
    computeContextsTime = (enTime - stTime);
    if (logLevel > 0) Helpers::WriteTime("Compute Contexts time", computeContextsTime);
    stTime = std::chrono::steady_clock::now();
    if (supervised)
        conceptifier.computeClosures(dataset, features, contexts, inliers, true, trainClosures);
    else conceptifier.computeClosuresUnsupervised(dataset, features, contexts, trainClosures);
    enTime = std::chrono::steady_clock::now();
    computeTrainClosuresTime = (enTime - stTime);
    if (logLevel > 0) Helpers::WriteTime("Compute Train Closures time", computeTrainClosuresTime);
    stTime = std::chrono::steady_clock::now();
    if (supervised)
        conceptifier.computeClosuresTest(testset, features, contexts, inliers, true, testClosures);
    else conceptifier.computeClosuresTestUnsupervised(testset, features, contexts, testClosures);
    enTime = std::chrono::steady_clock::now();
    computeTestClosuresTime = (enTime - stTime);
    if (logLevel > 0) Helpers::WriteTime("Compute Test Closures time", computeTestClosuresTime);
    
    contexts.clear();
    
    int maxEpochsCount = settings.get<int>("MaxTrainEpochs");
    std::vector<Eigen::VectorXd> outdegs;
    std::vector<double> gammas;
    std::vector<double> outWeights;
    double learningRate = settings.get<double>("LearningRate");
    double momentum = settings.get<double>("Momentum");
    double stopThreshold = settings.get<double>("TrainStopThreshold");
    //int maxGammaEpochsCount = settings.get<int>("MaxGammaTrainEpochs");
    //double gammaLearningRate = settings.get<double>("GammaLearningRate");
    //double gammaMomentum = settings.get<double>("GammaMomentum");
    //double gammaStopThreshold = settings.get<double>("GammaTrainStopThreshold");
    size_t trainedEpochs = 0;
    std::string gammaValues = settings.get<std::string>("GammaValues");
    if (gammaValues == "train") throw std::runtime_error("Gamma training is no more supported.");
    if (maxEpochsCount > 0 && supervised) {
        auto outdegfun = [](double x, double gamma) { return std::exp(-std::pow(gamma * x, 2)); };
        std::random_device randomDevice;
        std::mt19937 engine(randomDevice());
        std::uniform_real_distribution<> distribution(0, 1);
        //COMPUTE GAMMAS AND OUTDEGS
        if (gammaValues == "random") {
            for (int agendaID = 0; agendaID < nagendas; ++agendaID) {
                gammas.push_back(distribution(engine));
            }   
        } else {
            auto vals = Helpers::split(gammaValues, ',');
            for (auto& s : vals) Helpers::trim(s);
            if (vals.size() == 1) {
                double v = std::stod(vals[0]);
                for (int agendaID = 0; agendaID < nagendas; ++agendaID) gammas.push_back(v);
            } else {
                for (int agendaID = 0; agendaID < nagendas; ++agendaID)
                    gammas.push_back(std::stod(vals[agendaID]));
            }
        }
        stTime = std::chrono::steady_clock::now();
        losses::getOutdegsArray(trainClosures, outdegs, gammas, outdegfun);
        enTime = std::chrono::steady_clock::now();
        computeGammasTime = enTime - stTime;
        if (logLevel > 0) Helpers::WriteTime("Compute Gammas and Outdegs time", computeGammasTime);
        //TRAIN
        GradientDescent trainer = GradientDescent(nagendas, losses::getTrainLoss(outdegs, inliers), 
                                                    losses::getTrainGradient(outdegs, inliers),
                                                    learningRate, momentum, stopThreshold);
        stTime = std::chrono::steady_clock::now();
        trainedEpochs = trainer.train(maxEpochsCount, true);
        enTime = std::chrono::steady_clock::now();
        computeTrainingTime = enTime - stTime;
        if (logLevel > 0) Helpers::WriteTime("Compute Training time", computeTrainingTime);
        for (auto v : trainer.getWeights())
            outWeights.push_back(v);
    }
    
    
    std::ofstream output(settings.get<std::string>("OutputFile"));
    output << "{\n";
    Helpers::json_write_pair<int>(output, "nobjs", nobjs); output << ",\n";
    Helpers::json_write_pair<int>(output, "nagendas", nagendas); output << ",\n";
    Helpers::json_write_pair<size_t>(output, "trainedEpochs", trainedEpochs); output << ",\n";
    Helpers::json_write_pair(output, "trainClosures", trainClosures); output << ",\n";
    Helpers::json_write_pair(output, "testClosures", testClosures); output << ",\n";
    Helpers::json_write_pair(output, "gammas", gammas); output << ",\n";
    Helpers::json_write_pair(output, "weights", outWeights); output << ",\n";
    
    Helpers::json_write_pair(output, "computeContextsTime", computeContextsTime.count()); output << ",\n";
    Helpers::json_write_pair(output, "computeTrainClosuresTime", computeTrainClosuresTime.count()); output << ",\n";
    Helpers::json_write_pair(output, "computeTestClosuresTime", computeTestClosuresTime.count()); output << ",\n";
    Helpers::json_write_pair(output, "computeGammasTime", computeGammasTime.count()); output << ",\n";
    Helpers::json_write_pair(output, "computeTrainingTime", computeTrainingTime.count()); output << "\n";
    output << "}";
}

int main(int argc, char **argv)
{
	int currentArg = 1;
    Settings settings;
    while (currentArg < argc) {
        if (strcmp(argv[currentArg], "--train-set") == 0) {
            if (currentArg + 1 >= argc) throw std::invalid_argument("Missing train set file in command line input.");
            settings.set("TrainSet", argv[++currentArg]);
        } else if (strcmp(argv[currentArg], "--test-set") == 0) {
            if (currentArg + 1 >= argc) throw std::invalid_argument("Missing test set file in command line input.");
            settings.set("TestSet", argv[++currentArg]);
        } else if (strcmp(argv[currentArg], "--settings-file") == 0) {
            if (currentArg + 1 >= argc) throw std::invalid_argument("Missing setting file in command line input.");
            settings.ReadFile(argv[++currentArg]);
        } else if (strcmp(argv[currentArg], "--output-file") == 0) {
            if (currentArg + 1 >= argc) throw std::invalid_argument("Missing output file in command line input.");
            settings.set("OutputFile", argv[++currentArg]);
        } else if (strcmp(argv[currentArg], "--uniform-assigner-bins") == 0) {
            if (currentArg + 1 >= argc) throw std::invalid_argument("Missing uniform assigner bins file in command line input.");
            settings.set("UniformAssignerBins", argv[++currentArg]);
        } else if (strcmp(argv[currentArg], "--features") == 0) {
            if (currentArg + 1 >= argc) throw std::invalid_argument("Missing features in command line input.");
            settings.set("Features", argv[++currentArg]);
        } else if (strcmp(argv[currentArg], "--value-assigners") == 0) {
            if (currentArg + 1 >= argc) throw std::invalid_argument("Missing value assigners in command line input.");
            settings.set("ValueAssigners", argv[++currentArg]);
        } else if (strcmp(argv[currentArg], "--outlier-column") == 0) {
            if (currentArg + 1 >= argc) throw std::invalid_argument("Missing outlier column in command line input.");
            settings.set("OutlierColumn", argv[++currentArg]);
        } else if (strcmp(argv[currentArg], "--max-train-epochs") == 0) {
            if (currentArg + 1 >= argc) throw std::invalid_argument("Missing train epochs in command line input.");
            settings.set("MaxTrainEpochs", argv[++currentArg]);
        } else if (strcmp(argv[currentArg], "--learning-rate") == 0) {
            if (currentArg + 1 >= argc) throw std::invalid_argument("Missing learning rate in command line input.");
            settings.set("LearningRate", argv[++currentArg]);
        } else if (strcmp(argv[currentArg], "--momentum") == 0) {
            if (currentArg + 1 >= argc) throw std::invalid_argument("Missing momentum in command line input.");
            settings.set("Momentum", argv[++currentArg]);
        } else if (strcmp(argv[currentArg], "--train-stop-threshold") == 0) {
            if (currentArg + 1 >= argc) throw std::invalid_argument("Missing train stop threshold in command line input.");
            settings.set("TrainStopThreshold", argv[++currentArg]);
        } else if (strcmp(argv[currentArg], "--gamma-max-train-epochs") == 0) {
            if (currentArg + 1 >= argc) throw std::invalid_argument("Missing gamma train epochs in command line input.");
            settings.set("MaxGammaTrainEpochs", argv[++currentArg]);
        } else if (strcmp(argv[currentArg], "--gamma-learning-rate") == 0) {
            if (currentArg + 1 >= argc) throw std::invalid_argument("Missing gamma learning rate in command line input.");
            settings.set("GammaLearningRate", argv[++currentArg]);
        } else if (strcmp(argv[currentArg], "--gamma-momentum") == 0) {
            if (currentArg + 1 >= argc) throw std::invalid_argument("Missing gamma momentum in command line input.");
            settings.set("GammaMomentum", argv[++currentArg]);
        } else if (strcmp(argv[currentArg], "--gamma-train-stop-threshold") == 0) {
            if (currentArg + 1 >= argc) throw std::invalid_argument("Missing gamma train stop threshold in command line input.");
            settings.set("GammaTrainStopThreshold", argv[++currentArg]);
        } else if (strcmp(argv[currentArg], "--log-level") == 0) {
            if (currentArg + 1 >= argc) throw std::invalid_argument("Missing log level in command line input.");
            settings.set("ConceptifierLogLevel", argv[++currentArg]);
        } else if (strcmp(argv[currentArg], "--gamma-value") == 0) {
            if (currentArg + 1 >= argc) throw std::invalid_argument("Missing gamma value in command line input.");
            settings.set("GammaValues", argv[++currentArg]);
        } else if (strcmp(argv[currentArg], "--unsupervised") == 0) {
            settings.set("Supervised", "false");
            ++currentArg;
        } else if (strcmp(argv[currentArg], "--supervised") == 0) {
            settings.set("Supervised", "true");
            ++currentArg;
        } else if (strcmp(argv[currentArg], "--only-singleton-contexts") == 0) {
            settings.set("OnlySingletonContexts", "true");
            ++currentArg;
        } else if (strcmp(argv[currentArg], "--compute-doubleton-contexts") == 0) {
            settings.set("OnlySingletonContexts", "false");
            ++currentArg;
        } else if (strcmp(argv[currentArg], "--skip-full-lattice") == 0) {
            settings.set("SkipFullLattice", "true");
            ++currentArg;
        } else if (strcmp(argv[currentArg], "--compute-full-lattice") == 0) {
            settings.set("SkipFullLattice", "false");
            ++currentArg;
        }
        ++currentArg;
    }
    try{
        execute(settings);
    } catch (std::exception& e) {
        std::cout<<e.what()<<std::endl;
        return 1;
    }
	return 0;
}
