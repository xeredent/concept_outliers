#!/usr/bin/env python3
import pandas
import numpy
import math
import matplotlib
import sklearn.metrics
from sklearn.model_selection import train_test_split
import subprocess
import json
import time
import datetime
import os
import random

"""
default_outdeg_function Computes the outlier degree of an element given the
cardinality of its smallest concept in a lattice.

:param1 card: The cardinality of the smallest concept to which the object belongs.
:param2 alpha: A parameter by which the cardinality is multiplied.
:return: The outlier degree.
"""
def default_outdeg_function(card, alpha):
   return math.exp(-((alpha*card)**2))

def compute_outdegs(closures, gammas):
  outdegs = []
  for i in range(len(closures)):
    outdegs.append(numpy.exp(-((numpy.array(closures[i]) * gammas[i]) ** 2)))
  return outdegs

def compute_predictions(w, features, x_test, predict_agendas = None):
  Wp = sum([x if x > 0 else 0 for x in w ])
  Wm = sum([x if x < 0 else 0 for x in w ])
  Wpm = Wp + Wm
  predictions = []
  for i in range(len(x_test)):
    ycap = (sum([w[j] * predict_agendas[j][i] for j in range(len(predict_agendas))]) - Wm) / Wpm
    predictions.append(ycap)
  return predictions

def compute_auc_data(y_test, predictions):
  #fpr, tpr, thresholds = sklearn.metrics.roc_curve(y_test, predictions)
  #matplotlib.pyplot.plot(fpr, tpr, marker='.', label='Model')
  #matplotlib.pyplot.xlabel('False Positive Rate')
  #matplotlib.pyplot.ylabel('True Positive Rate')
  #matplotlib.pyplot.legend()
  #matplotlib.pyplot.show()
  try:
      return sklearn.metrics.roc_auc_score(y_test, predictions)
  except:
      return 0



class Test(object):
  def __init__(self, testname = "", workingDirectory = ".", trainset_name = "train.csv", \
               testset_name = "test.csv", closures_output_name = "output.json", settings = {}):
    self.testname = testname
    self.workingDirectory = workingDirectory
    os.makedirs(workingDirectory, exist_ok = True)
    self.trainset_name = trainset_name
    self.testset_name = testset_name
    self.closures_output_name = closures_output_name
    self.data = None
    self.outliers = None
    self.weights = None
    self.features = []
    self.settings = settings

  def load_dataset(self):
    pass

  def getBoolSetting(self, k):
      if k not in self.settings:
          return False
      s = self.settings[k]
      return s == True or s == "true" or s == "True" or s == "TRUE"

  def prepare_dataset(self):
    y = numpy.array(self.outliers)
    x_train, x_test, y_train, y_test = train_test_split(self.data, y, train_size = 0.8, stratify=y)
    x_train["y"] = y_train
    x_train.to_csv(self.workingDirectory + "/" + self.trainset_name, sep = ",")
    x_test["y"] = y_test
    x_test.to_csv(self.workingDirectory + "/" + self.testset_name, sep = ",")
    self.x_train = x_train; self.x_test = x_test; self.y_train = y_train; self.y_test = y_test
    if self.settings["GammaValue"] == "adaptive":
        isuniform = lambda x : not(hasattr(self, "uniform_features") and x >= len(self.uniform_features))
        nbins = lambda x : self.settings["UniformAssignerBins"] if isuniform(x) else x_train[self.features[x]].nunique()
        size = len(self.x_train.index); outrate = sum(self.y_train) / size
        accumForFull = 1.0; gammas = []
        gammafun = lambda n : (n / size)
        if self.settings["OnlySingletonContexts"] in ["true", "True", "TRUE"]:
            for f1 in range(len(self.features)):
                b1 = nbins(f1)
                accumForFull = accumForFull * b1
                gammas.append(gammafun(b1))
        else:    
            for f1 in range(len(self.features)):
                b1 = nbins(f1);
                accumForFull = accumForFull * b1
                for f2 in range(f1, len(self.features)):
                    b2 = nbins(f2)
                    gammas.append(gammafun(b1 * b2))
        if self.settings["SkipFullLattice"] not in ["true", "True", "TRUE"]:
            gammas.append(gammafun(accumForFull))
        self.settings["GammaValue"] = ",".join([str(g) for g in gammas])
                

  def computeClosures(self):
    with open(self.workingDirectory + "/settings.txt", "w") as file:
      for k in self.settings:
        file.write(str(k) + " = " + str(self.settings[k]) + "\n")
    args = ["./concept_outliers", \
            "--train-set", self.workingDirectory + "/" + self.trainset_name,\
            "--test-set",  self.workingDirectory + "/" + self.testset_name, \
            "--output-file", self.workingDirectory + "/" +self.closures_output_name,\
            "--features", ','.join(self.features),\
            "--settings-file", self.workingDirectory + "/settings.txt" ]
    subprocess.run(args, check = True)
    with open(self.workingDirectory + "/" +self.closures_output_name, "r") as fil:
      self.outputData = json.load(fil)
    self.trainClosures = numpy.array(self.outputData["trainClosures"])
    self.testClosures = numpy.array(self.outputData["testClosures"])
    self.nagendas = self.outputData["nagendas"]
    self.trainedEpochs = self.outputData["trainedEpochs"]
    self.weights = self.outputData["weights"]
    if len(self.weights) == 0:
        self.weights = [1 for i in range(self.nagendas)]
    self.gammas = self.outputData["gammas"]
    if len(self.gammas) == 0:
        self.gammas = [random.random() for i in range(self.nagendas)]
    self.computeContextsTime = self.outputData["computeContextsTime"]
    self.computeTrainClosuresTime = self.outputData["computeTrainClosuresTime"]
    self.computeTestClosuresTime = self.outputData["computeTestClosuresTime"]
    self.computeGammasTime = self.outputData["computeGammasTime"]
    self.computeTrainingTime = self.outputData["computeTrainingTime"]
    self.outdegs = compute_outdegs(self.trainClosures, self.gammas)
    self.testAgendas = compute_outdegs(self.testClosures, self.gammas)

  def computeContributions(self):
      self.contributions = []
      outlierCount = sum(self.y_train)
      nobjs = len(self.x_train)
      objsVals = [sum([self.outdegs[agendaID][i] * self.weights[agendaID] for agendaID in range(self.nagendas)]) for i in range(nobjs) ]
      for agendaID in range(self.nagendas):
          avg = sum([self.y_train[i] * self.outdegs[agendaID][i] * self.weights[agendaID]/objsVals[i] if objsVals[i] != 0 else 0 for i in range(len(self.x_train))]) / outlierCount
          self.contributions.append(avg)
      self.contributionsSum = sum(self.contributions)

  def computePredictions(self):
    self.predictions = compute_predictions(self.weights, self.features, self.x_test, self.testAgendas)
    self.aucScore = compute_auc_data(self.y_test, self.predictions)
    print("AUC: ", self.aucScore)

  def saveResults(self, fname = "INFO.TXT"):
    f = open(self.workingDirectory + "/" + fname, "w")
    f.write("[General]\nTest " + self.testname + ".\n")
    f.write("Date and time: " + datetime.datetime.now().strftime("%d/%m/%Y %H:%M:%S") + "\n" )
    f.write("Working directory: " + self.workingDirectory  + "\n")
    f.write("Trainset: " + self.trainset_name + "  (sz = " + str(len(self.x_train.index)) + ")\n" )
    f.write("Testset: " + self.testset_name + "  (sz = " + str(len(self.x_test.index)) + ")\n")
    f.write("Closures file: " + self.closures_output_name  + "\n")
    f.write("Number of agendas: " + str(self.nagendas) + "\n")
    f.write("Number of features: " + str(len(self.features)) + "\n")
    f.write("Trained epochs: " + str(self.trainedEpochs) + "\n")
    f.write("AUC: " + str(self.aucScore) + "\n")
    f.write("\n[Time]\n")
    f.write("Contexts:       " + str(self.computeContextsTime) + "s\n")
    f.write("Train Closures: " + str(self.computeTrainClosuresTime) + "s\n")
    f.write("Test Closures:  " + str(self.computeTestClosuresTime) + "s\n")
    f.write("Gammas:         " + str(self.computeGammasTime) + "s\n")
    f.write("Training:       " + str(self.computeTrainingTime) + "s\n")
    f.write("Total:          " + str(self.computeContextsTime + self.computeTrainClosuresTime + self.computeTestClosuresTime + self.computeGammasTime + self.computeTrainingTime) + "s\n" )
    f.write("\n[Parameters]\n")
    for k in self.settings:
      f.write(str(k) + " = " + str(self.settings[k]) + "\n")
    f.write("\n[Data]\n")
    f.write("Weights: \n" )
    if self.settings["ComputeContributions"]:
        if self.getBoolSetting("OnlySingletonContexts"):
            for i in range(len(self.features)):
                f.write("\t%20s: %g (%g%%)\n" % (self.features[i], self.weights[i], self.contributions[i] / self.contributionsSum * 100)  )
        else:
            i = 0
            for i1 in range(len(self.features)):
                f1 = self.features[i1]
                for i2 in range(i1, len(self.features)):
                    f2 = self.features[i2]
                    f.write("\t%20s: %g (%g%%)\n" % (f1 + "-" + f2 if i1 != i2 else f1, self.weights[i], self.contributions[i] / self.contributionsSum * 100)  )
                    i = i + 1
            if not self.getBoolSetting("SkipFullLattice"):
                f.write("\t%20s: %g (%g%%)\n" % ("full", self.weights[-1], self.contributions[-1] / self.contributionsSum * 100))
    else:
        f.write(str(self.weights))
    f.close()
    
  def execute(self):
    startTime = time.time()
    self.load_dataset()
    self.prepare_dataset()
    self.computeClosures()
    if self.settings["ComputeContributions"]:
        self.computeContributions()
    self.computePredictions()
    endTime = time.time()
    self.time = endTime - startTime
    print("TEST completed. Results in " + self.workingDirectory + ". Total time " + str(self.time) + " seconds.")
    self.saveResults()
    
  def executeRange(self, testbins):
      self.load_dataset()
      self.prepare_dataset()
      os.makedirs(self.workingDirectory + "/outputs", exist_ok = True)
      os.makedirs(self.workingDirectory + "/reports", exist_ok = True)
      testdata = { "tests" : [], "best" : None, "bestAUC" : 0 }
      for nbins in testbins:
          self.settings["UniformAssignerBins"] = nbins
          self.closures_output_name = "outputs/nbins_" + str(nbins) + ".json"
          report_output_name = "reports/nbins_" + str(nbins) + ".txt"
          print("Starting test " + self.testname + " nbins " + str(nbins))
          startTime = time.time()
          self.computeClosures()
          if self.settings["ComputeContributions"]:
              self.computeContributions()
          self.computePredictions()
          self.saveResults(report_output_name)
          endTime = time.time()
          self.time = endTime - startTime
          print("TEST completed. Results in " + self.workingDirectory + ". Total time " + str(self.time) + " seconds.")
          test = { "testname" : self.testname, "nbins" : nbins, "auc" : self.aucScore }
          testdata["tests"].append(test)
          if self.aucScore > testdata["bestAUC"]:
              testdata["best"] = test
              testdata["bestAUC"] = self.aucScore
      with open(self.workingDirectory + "/results.json", "w") as fil:
          json.dump(testdata, fil)
      return testdata
      
    
import scipy.io
class TestMat(Test):
  def __init__(self, matdataset, testname = "", workingDirectory = ".", trainset_name = "train.csv", \
               testset_name = "test.csv", closures_output_name = "output.json", settings = {}):
    self.matdataset = matdataset
    super().__init__(testname = testname, workingDirectory = workingDirectory,\
                     trainset_name = trainset_name, testset_name = testset_name,\
                     closures_output_name = closures_output_name, settings = settings)
    
  def load_dataset(self):
    mat = scipy.io.loadmat(self.matdataset)
    self.features = ["att" + str(i+1) for i in range(len(mat["X"][0]))]
    self.data = pandas.DataFrame(mat["X"], columns = self.features)
    #print(self.data)
    self.outliers = mat["y"]
    
class TestArff(Test):
  def __init__(self, matdataset, testname = "", workingDirectory = ".", trainset_name = "train.csv", \
               testset_name = "test.csv", closures_output_name = "output.json", settings = {}):
    self.matdataset = matdataset
    super().__init__(testname = testname, workingDirectory = workingDirectory,\
                     trainset_name = trainset_name, testset_name = testset_name,\
                     closures_output_name = closures_output_name, settings = settings)
    
  def load_dataset(self):
    arff_data = scipy.io.arff.loadarff(self.matdataset)
    self.data = pandas.DataFrame(arff_data[0])
    self.features = ["att" + str(i+1) for i in range(len(self.data.columns)-2)]
    outlierName = "outlier" if "outlier" in self.data.columns else "Outlier"
    self.outliers = numpy.array([(1 if s ==b'yes' else 0) for s in self.data[outlierName]])
    del self.data[outlierName]
    del self.data["id"]
    self.data.columns = self.features
    
import os.path
class TestCustom(Test):
    def __init__(self, customfile, testname = "", workingDirectory = ".", trainset_name = "train.csv", \
             testset_name = "test.csv", closures_output_name = "output.json", settings = {}):
        self.customfile = customfile
        self.dataset_path = ""; self.uniform_features = []; self.value_features = []; self.all_features = []
        self.sample_rate = 1.0
        self.outlier_column_name= ""
        super().__init__(testname = testname, workingDirectory = workingDirectory,\
                     trainset_name = trainset_name, testset_name = testset_name,\
                     closures_output_name = closures_output_name, settings = settings)
          
    def load_dataset(self):
        with open(self.customfile, "r") as f:
            for line in f:
                cmd, val = tuple([s.strip() for s in line.split("=")])
                if cmd == "dataset":
                    self.dataset_path = val.strip()
                elif cmd == "uniform":
                    self.uniform_features = [s.strip() for s in val.split(",")]
                elif cmd == "value":
                    self.value_features = [s.strip() for s in val.split(",")]
                elif cmd == "outlier":
                    self.outlier_column_name = val.strip()
                elif cmd == "sample":
                    self.sample_rate = float(val)
        self.all_features = [f for f in self.uniform_features]
        self.all_features.extend([f for f in self.value_features])
        self.features = ["att" + str(i+1) for i in range(len(self.uniform_features) + len(self.value_features))]
        self.data = pandas.read_csv(os.path.dirname(self.customfile) + "/" + self.dataset_path)
        if self.sample_rate != 1.0:
            self.data = self.data.groupby(self.outlier_column_name, group_keys = False).apply(lambda x : x.sample(frac = self.sample_rate, axis = 0))
            #self.data = self.data.sample(frac = self.sample_rate, axis = 0)
        self.outliers = self.data[self.outlier_column_name]
        self.data.rename(columns = { self.all_features[i] : "att" + str(i + 1) for i in range(len(self.all_features))}, inplace = True)
        self.data = self.data[self.features]
        self.settings["ValueAssigners"] = ",".join(["att" + str(i + 1) for i in range(len(self.uniform_features), len(self.all_features))])
    
import h5py
class TestMatlab73(Test):
  def __init__(self, matdataset, testname = "", workingDirectory = ".", trainset_name = "train.csv", \
               testset_name = "test.csv", closures_output_name = "output.json", settings = {}):
    self.matdataset = matdataset
    super().__init__(testname = testname, workingDirectory = workingDirectory,\
                     trainset_name = trainset_name, testset_name = testset_name,\
                     closures_output_name = closures_output_name, settings = settings)
    
  def load_dataset(self):
    with h5py.File(self.matdataset, 'r') as mat:
        mX = numpy.array(mat["X"]).transpose()
        self.features = ["att" + str(i+1) for i in range(len(mX[0]))]
        self.data = pandas.DataFrame(numpy.array(mX), columns = self.features)
        self.outliers = numpy.array(mat["y"]).transpose()
    
settings = {
        "Supervised" : "true",
        "UniformAssignerBins" : 50,
        "MaxTrainEpochs" : 2000,
        "LearningRate" : 0.01,
        "Momentum" : 0.01,
        "TrainStopThreshold" : 0.001,
        "OnlySingletonContexts" : "false",
        "SkipFullLattice" : "false",
        "ConceptifierLogLevel" : 3,
        "ComputeContributions" : False,
        "GammaValue" : "adaptive"
    }
testfolder = "datasets"

import sys
import getopt
opts, args = getopt.getopt(sys.argv[1:], "hd:se:l:m:t:u",\
                           ["help", "directory=", "startbin=", "endbin=", \
                            "increment=", "learning-rate=", "momentum=", \
                            "epochs=", "unsupervised", "supervised", "test-amount=", \
                            "train-stop-threshold=", "skipfull", "noskipfull", "onlysingletons",
                            "compute-contributions", "gamma="])
startbin = 20
endbin = 100
increment = 5
testamount = 1
for opt, arg in opts:
    if opt in ("-h", "--help"):
        print("Usage: coutliers.py [OPTIONS]")
        print("The options can be the following:")
        print("\t-h, --help")
        print("\t\tShows this helping message.")
        print("")
        print("\t-d <path>, --directory <path>      (DEFAULT `datasets')")
        print("\t\tSpecifies a directory containing the datasets. The datasets must have a specific format, i.e., matlab and arff datasets whose outlier column has a specific name (`y' and `outlier', respectively).")
        print("")
        print("\t-s --onlysingletons                (DEFAULT FALSE)")
        print("\t\tConsiders only singleton sets to generate the contexts.")
        print("")
        print("\t--skipfull, --noskipfull           (DEFAULT NO SKIP)")
        print("\t\tSignals whether to skip the full lattice generation.")
        print("")
        print("\t-e <amount>, --epochs <amount>     (DEFAULT 2000)")
        print("\t\tThe maximum amount of training epochs in supervised mode.")
        print("")
        print("\t-u, --unsupervised, --supervised   (DEFAULT SUPERVISED)")
        print("\t\tRun the (un)supervised version of the algorithm.")
        print("")
        print("\t-l <rate>, --learning-rate <rate>  (DEFAULT 0.01)")
        print("\t\tSets the learning rate for the supervised training algorithm.")
        print("")
        print("\t-m <amount>, --momentum <amount>   (DEFAULT 0.01)")
        print("\t\tSets the momentum for the supervised training algorithm.")
        print("")
        print("\t-t <amount>, --train-stop-threshold <amount>   (DEFAULT 0.001)")
        print("\t\tSets the loss gain threshold under which training is stopped.")
        print("")
        print("\t--startbin <amount>                (DEFAULT 20)")
        print("\t\tSets the starting bin for the tests. Test bins are generated in range(startbin, endbin + 1, increment).")
        print("")
        print("\t--endbin <amount>                  (DEFAULT 100)")
        print("\t\tSets the ending bin for the tests. Test bins are generated in range(startbin, endbin + 1, increment).")
        print("")
        print("\t--increment <amount>               (DEFAULT 5)")
        print("\t\tSets the bin increment for the tests. Test bins are generated in range(startbin, endbin + 1, increment).")
        print("")
        print("\t--test-amount <amount>             (DEFAULT 1)")
        print("\t\tSets the number of tests per dataset to perform. Each test performs a different train/test split.")
        print("")
        print("\t--compute-contributions             (DEFAULT FALSE)")
        print("\t\tFor each test, computes and store the contributions given by each feature. It is relatively slow, as it is implemented in python.")
        print("")
        print("\t--gamma <gamma>|<gamma_list>|random|adaptive  (DEFAULT GAMMA is adaptive)")
        print("\t\tIf one value is provided, that value is used for all the agendas, otherwise, if a list is provided, each agenda uses a different gamma.")
        sys.exit(0)
    elif opt in ("-d", "--directory"):
        testfolder = arg
    elif opt in ("-s", "--onlysingletons"):
        settings["OnlySingletonContexts"] = "true"
    elif opt == "--skipfull":
        settings["SkipFullLattice"] = "true"
    elif opt == "--noskipfull":
        settings["SkipFullLattice"] = "false"
    elif opt in ("-e", "--epochs"):
        settings["MaxTrainEpochs"] = int(arg)
    elif opt in ("-u", "--unsupervised"):
        settings["Supervised"] = "false"
    elif opt in ("--supervised"):
        settings["Supervised"] = "true"
    elif opt in ("-l", "--learning-rate"):
        settings["LearningRate"] = float(arg)
    elif opt in ("-m", "--momentum"):
        settings["Momentum"] = float(arg)
    elif opt in ("-t", "--train-stop-threshold"):
        settings["TrainStopThreshold"] = float(arg)
    elif opt == "--startbin":
        startbin = int(arg)
    elif opt == "--endbin":
        endbin = int(arg)
    elif opt == "--increment":
        increment = int(arg)
    elif opt == "--test-amount":
        testamount = int(arg)
    elif opt == "--compute-contributions":
        settings["ComputeContributions"] = False
    elif opt == "--fixed-gamma":
        settings["GammaValue"] = arg

outdata = {}
def writedata():
    with open("results.json", "w") as fil:
        json.dump(outdata, fil)
for file in os.listdir(os.fsencode(testfolder)):
    filename = os.fsdecode(file)
    testname = ""
    testtype = "mat"
    if filename.endswith(".csv"):
        continue
    if filename.endswith(".arff"):
        testname = filename[:-5]
        testtype = "arff"
    else:
        testname = filename[:-4]
    if filename.endswith(".txt"):
        testtype = "custom"
    testdata = { "name" : testname, "tests" : [], "best" : None, "bestAUC" : 0 }
    for attemptID in range(testamount):
        t1 = None
        if testtype == "mat":
            t1 = TestMat(testfolder + "/" + filename,
                         testname = testname, \
                         workingDirectory = "./tests/" + testname + "_" + str(attemptID),\
                         trainset_name = "train.csv", \
                         testset_name = "test.csv", \
                         closures_output_name = "output.json",
                         settings = settings)
            testdata["type"] = "mat"
        elif testtype == "arff":
            t1 = TestArff(testfolder + "/" + filename,
                         testname = testname, \
                         workingDirectory = "./tests/" + testname + "_" + str(attemptID),\
                         trainset_name = "train.csv", \
                         testset_name = "test.csv", \
                         closures_output_name = "output.json",
                         settings = settings)
            testdata["type"] = "arff"
        elif testtype == "custom":
            t1 = TestCustom(testfolder + "/" + filename,
                         testname = testname, \
                         workingDirectory = "./tests/" + testname + "_" + str(attemptID),\
                         trainset_name = "train.csv", \
                         testset_name = "test.csv", \
                         closures_output_name = "output.json",
                         settings = settings)
            testdata["type"] = "custom"
        try:
            tests = t1.executeRange(range(startbin, endbin + 1, increment))
        #try:
        #    pass
        except Exception as e:
            if testdata["type"] == "mat":
                t1 = TestMatlab73(testfolder + "/" + filename,
                             testname = testname, \
                             workingDirectory = "./tests/" + testname + "_" + str(attemptID),\
                             trainset_name = "train.csv", \
                             testset_name = "test.csv", \
                             closures_output_name = "output.json",
                             settings = settings)
                testdata["type"] = "mat73"
                tests = t1.executeRange(range(startbin, endbin + 1, increment))
            else:
                print(e)
        for t in tests["tests"]:
            t["attemptID"] = attemptID
        testdata["tests"].append(tests)
        if tests["bestAUC"] > testdata["bestAUC"]:
            testdata["best"] = tests["best"]
            testdata["bestAUC"] = tests["bestAUC"]
    outdata[testname] = testdata
    writedata()
