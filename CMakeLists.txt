cmake_minimum_required(VERSION 3.1 FATAL_ERROR)

set (CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_FLAGS "-Wall")
set(CMAKE_CXX_FLAGS_DEBUG "-g")
set(CMAKE_CXX_FLAGS_RELEASE "-O3 --march=native")

find_package (Eigen3 3.4 REQUIRED NO_MODULE)


include_directories("./concept_outliers/include")
include_directories("./rapidcsv")

add_executable (concept_outliers concept_outliers/src/Helpers.cpp concept_outliers/src/PartialContext.cpp concept_outliers/src/Settings.cpp concept_outliers/src/FeatureAssigner.cpp concept_outliers/src/Conceptifier.cpp concept_outliers/src/GradientDescent.cpp concept_outliers/src/main.cpp)

target_link_libraries (concept_outliers Eigen3::Eigen)

project("concept_outliers")
